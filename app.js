const express = require("express");
const fs = require("fs");
const cors = require("cors");
const app = express();
const port = 8080;

const exts = [".log", ".txt", ".json", ".yaml", ".xml", ".js"];
if (!fs.existsSync("./files")) {
  fs.mkdirSync("./files");
}

app.use(express.json());
app.use(cors());

app.use((req, res, next) => {
  const log = `${new Date().toLocaleString()} ${req.method} ${req.originalUrl}`;
  fs.appendFile("logs.log", log + "\n", function (error) {
    if (error) {
      return;
    }
  });
  next();
});

app.post("/api/files", checkExt, (req, res) => {
  if (!req.body.content) {
    res.status(400).send({ message: "Please specify 'content' parameter" });
    return;
  }

  if (fs.existsSync("files/" + req.body.filename)) {
    res.status(400).send({
      message: `File with '${req.body.filename}' filename already exist`,
    });
    return;
  } else {
    fs.writeFile(
      "files/" + req.body.filename,
      req.body.content,
      function (error) {
        if (error) {
          res.status(500).send({ message: "Server error" });
        } else {
          res.status(200).send({ message: "File created successfully" });
        }
      }
    );
  }
});

app.get("/api/files/:filename", checkExt, (req, res) => {
  fs.stat("files/" + req.params.filename, function (error, stats) {
    if (error) {
      res.status(400).send({
        message: `No file with '${req.params.filename}' filename found`,
      });
    } else {
      fs.readFile(
        "files/" + req.params.filename,
        "utf8",
        function (error, data) {
          if (error) {
            res.status(500).send({ message: "Server error" });
          } else {
            console.log(stats);
            res.status(200).send({
              message: "Success",
              filename: req.params.filename,
              content: data,
              extension: req.params.filename.split(".").pop(),
              uploadedDate: stats.ctime,
            });
          }
        }
      );
    }
  });
});
app.get("/api/files", (req, res) => {
  fs.readdir("files/", (error, files) => {
    if (error) {
      res.status(500).send({ message: "Server error" });
    } else {
      res.status(200).send({ message: "Success", files });
    }
  });
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});

function checkExt(req, res, next) {
  const filename = req.params.filename || req.body.filename;
  console.log(filename);
  if (filename) {
    const ok = exts.some((ext) => filename.endsWith(ext));
    if (ok) {
      next();
    } else {
      res.status(400).send({ message: "Wrong filename" });
    }
  } else {
    res.status(400).send({ message: "Filename is missing" });
  }
}
